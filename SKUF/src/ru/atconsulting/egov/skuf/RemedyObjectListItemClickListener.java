package ru.atconsulting.egov.skuf;

import ru.atconsulting.egov.skuf.remedy.artifacts.RemedyObject;

public interface RemedyObjectListItemClickListener {
	
	public void onListItemClick(RemedyObject object);
	
}
