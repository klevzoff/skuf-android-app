package ru.atconsulting.egov.skuf.util;

import ru.atconsulting.egov.skuf.Constants;
import ru.atconsulting.egov.skuf.remedy.RemedyUser;
import android.content.Intent;
import android.os.Bundle;

public class RemedyUserBundler {
	
	public static void addToBundle(RemedyUser user, Bundle bundle) {
		bundle.putString(Constants.STR_KEY_USERNAME, user.getUsername());
		bundle.putString(Constants.STR_KEY_PASSWORD, user.getPassword());
		RemedySupportGroupBundler.addToBundle(user.getGroups(), bundle, Constants.STR_KEY_GROUPS);
	}
	
	public static void addToIntent(RemedyUser user, Intent intent) {
		intent.putExtra(Constants.STR_KEY_USERNAME, user.getUsername());
		intent.putExtra(Constants.STR_KEY_PASSWORD, user.getPassword());
		RemedySupportGroupBundler.addToIntent(user.getGroups(), intent, Constants.STR_KEY_GROUPS);
	}
	
	public static RemedyUser readFromBundle(Bundle bundle) {
		RemedyUser user = new RemedyUser();
		user.setUsername(bundle.getString(Constants.STR_KEY_USERNAME));
		user.setPassword(bundle.getString(Constants.STR_KEY_PASSWORD));
		user.setGroups(RemedySupportGroupBundler.readFromBundle(bundle, Constants.STR_KEY_GROUPS));
		return user;
	}
	
	public static RemedyUser readFromIntent(Intent intent) {
		RemedyUser user = new RemedyUser();
		user.setUsername(intent.getStringExtra(Constants.STR_KEY_USERNAME));
		user.setPassword(intent.getStringExtra(Constants.STR_KEY_PASSWORD));
		user.setGroups(RemedySupportGroupBundler.readFromIntent(intent, Constants.STR_KEY_GROUPS));
		return user;
	}
}
