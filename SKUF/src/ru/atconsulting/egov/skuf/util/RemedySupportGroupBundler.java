package ru.atconsulting.egov.skuf.util;

import java.util.ArrayList;
import java.util.List;

import ru.atconsulting.egov.skuf.remedy.RemedySupportGroup;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;

public class RemedySupportGroupBundler {
	
	private static ArrayList<Bundle> makeBundleList(List<RemedySupportGroup> groups) {
		ArrayList<Bundle> blist = new ArrayList<Bundle>(groups.size());
		for (RemedySupportGroup group : groups) {
			Bundle b = new Bundle();
			b.putCharSequence("id", group.getId());
			b.putCharSequence("name", group.getName());
			b.putCharSequence("organization", group.getOrganization());
			b.putCharSequence("company", group.getCompany());
			blist.add(b);
		}
		return blist;
	}
	
	public static void addToBundle(List<RemedySupportGroup> groups, Bundle bundle, String key) {
		bundle.putParcelableArrayList(key, makeBundleList(groups));
	}
	
	public static void addToIntent(List<RemedySupportGroup> groups, Intent intent, String key) {
		intent.putExtra(key, makeBundleList(groups));
	}
	
	private static List<RemedySupportGroup> parseBundleList(List<Parcelable> blist) {
		List<RemedySupportGroup> groups = new ArrayList<RemedySupportGroup>(blist.size());
		for (Parcelable b : blist) {
			RemedySupportGroup group = new RemedySupportGroup();
			group.setId(((Bundle)b).getCharSequence("id").toString());
			group.setName(((Bundle)b).getCharSequence("name").toString());
			group.setOrganization(((Bundle)b).getCharSequence("organization").toString());
			group.setCompany(((Bundle)b).getCharSequence("company").toString());
			groups.add(group);
		}
		return groups;
	}
	
	public static List<RemedySupportGroup> readFromBundle(Bundle bundle, String key) {
		return parseBundleList(bundle.getParcelableArrayList(key));
	}
	
	public static List<RemedySupportGroup> readFromIntent(Intent intent, String key) {
		return parseBundleList(intent.getParcelableArrayListExtra(key));
	}
}

