package ru.atconsulting.egov.skuf.util;

import java.util.HashMap;
import java.util.Map;

import ru.atconsulting.egov.skuf.RemedyObjectDetailFragment;
import ru.atconsulting.egov.skuf.remedy.RemedyObjectType;
import ru.atconsulting.egov.skuf.remedy.artifacts.RemedyObject;

public class RemedyObjectDetailFragmentFactory {
	
	private Map<RemedyObjectType, RemedyObjectDetailFragment<? extends RemedyObject>> mMap;
	private static RemedyObjectDetailFragmentFactory mFactory = new RemedyObjectDetailFragmentFactory();
	
	private RemedyObjectDetailFragmentFactory() {
		mMap = new HashMap<RemedyObjectType, RemedyObjectDetailFragment<? extends RemedyObject>>();
	}
	
	public static RemedyObjectDetailFragmentFactory getInstance() {
		return mFactory;
	}
	
	public synchronized void register(RemedyObjectType type, RemedyObjectDetailFragment<? extends RemedyObject> fragment) {
		mMap.put(type, fragment);
	}
	
	public RemedyObjectDetailFragment<? extends RemedyObject> createFragment(RemedyObjectType type) {
		RemedyObjectDetailFragment<? extends RemedyObject> fr = mMap.get(type);
		if (fr == null) {
			return null;
		}
		return fr.createFragment();
	}
	
}
