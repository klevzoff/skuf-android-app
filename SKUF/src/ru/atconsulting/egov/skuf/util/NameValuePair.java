package ru.atconsulting.egov.skuf.util;

public class NameValuePair {
	private String mName;
	private Object mValue;
	
	public NameValuePair() {
		
	}
	
	public NameValuePair(String name, Object value) {
		this.mName = name;
		this.mValue = value;
	}
	
	public String getName() {
		return mName;
	}
	public void setName(String mName) {
		this.mName = mName;
	}
	public Object getValue() {
		return mValue;
	}
	public void setValue(Object mValue) {
		this.mValue = mValue;
	}
}
