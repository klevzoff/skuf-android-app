package ru.atconsulting.egov.skuf.util;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SoapDateTimeParser {
	
	private static final Pattern pattern = Pattern.compile("(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)T(\\d\\d):(\\d\\d):(\\d\\d)([.](\\d{1,3}))?((?:([+]|[-])(\\d\\d):(\\d\\d))|Z)?");

	public static Date parse(String s) {
		if (s == null) {
			return null;
		}
		Matcher m = pattern.matcher(s);
		if (m.matches()) {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, Integer.parseInt(m.group(1)));
			cal.set(Calendar.MONTH, Integer.parseInt(m.group(2)) - 1);
			cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(m.group(3)));
			cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(m.group(4)));
			cal.set(Calendar.MINUTE, Integer.parseInt(m.group(5)));
			cal.set(Calendar.SECOND, Integer.parseInt(m.group(6)));
			if (m.group(7) != null) {
				if (!m.group(7).equals("")) {
					cal.set(Calendar.MILLISECOND, Integer.parseInt(String.format("%1$3", m.group(8)).replace(' ', '0')));
				}
			}
			if (m.group(9) != null) {
				if (!m.group(9).equals("")) {
					int offset = 0;
					if (!m.group(9).equals("Z")) {
						offset = (m.group(10).equals("-") ? -1 : 1) * (Integer.parseInt(m.group(11)) * 60 + Integer.parseInt(m.group(12))) * 60 * 1000;
					}
					cal.set(Calendar.ZONE_OFFSET, offset);
				}
			}
			return cal.getTime();
		} else {
			return null;
		}
	}
	
}
