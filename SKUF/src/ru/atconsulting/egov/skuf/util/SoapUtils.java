package ru.atconsulting.egov.skuf.util;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;
import org.kxml2.kdom.Node;
import org.xmlpull.v1.XmlPullParserException;

import ru.atconsulting.egov.skuf.AppError;
import ru.atconsulting.egov.skuf.BuildConfig;
import ru.atconsulting.egov.skuf.Constants;
import ru.atconsulting.egov.skuf.R;

import android.content.Context;
import android.util.Log;

public class SoapUtils {
	
	private static final Pattern pattern = Pattern.compile("(?:ARERR \\[(\\d+)\\]|ERROR \\((\\d+)\\))[:] (.*)");
	
	public static void makeAuthHeader(SoapSerializationEnvelope envelope, String namespace, String username, String password) {
		envelope.headerOut = new Element[1];
		Element h = new Element().createElement(namespace, "AuthenticationInfo");
		Element user = new Element().createElement(namespace, "userName");
		user.addChild(Node.TEXT, username);
		h.addChild(Node.ELEMENT, user);
		Element pass = new Element().createElement(namespace, "password");
		pass.addChild(Node.TEXT, password);
		h.addChild(Node.ELEMENT, pass);
		envelope.headerOut[0] = h;
	}
	
	public static Object makeCall(Context context, String URL, String namespace, String method, SoapSerializationEnvelope envelope) throws AppError {
		
		
		HttpTransportSE http = new HttpTransportSE(URL);
		http.debug = true;
		
		try {
			http.call(namespace + "/" + method, envelope);
			return envelope.getResponse();
		}
		catch (SoapFault e) {
			if (BuildConfig.DEBUG) {
				Log.e(Constants.LOG_TAG, "SOAP Fault", e);
				Log.d(Constants.LOG_TAG, http.requestDump);
				Log.d(Constants.LOG_TAG, http.responseDump);
			}
			throw parseSoapFault(e);
		}
		catch (IOException e) {
			if (BuildConfig.DEBUG) {
				Log.e(Constants.LOG_TAG, "IO Exception", e);
			}
			throw new AppError(context.getString(R.string.connection_error));
		}
		catch (XmlPullParserException e) {
			if (BuildConfig.DEBUG) {
				Log.e(Constants.LOG_TAG, "Parsing Exception while processing SOAP results", e);
				Log.d(Constants.LOG_TAG, http.requestDump);
				Log.d(Constants.LOG_TAG, http.responseDump);
			}
			throw new AppError(context.getString(R.string.parsing_error));
		}
	}
	
	public static AppError parseSoapFault(SoapFault fault) {
		Matcher matcher = pattern.matcher(fault.faultstring);
		int errnum;
		String msg;
		if (matcher.find()) {
			if (matcher.group(1) != null) {
				errnum = Integer.parseInt(matcher.group(1));
			} else {
				errnum = Integer.parseInt(matcher.group(2));
			}
			msg = matcher.group(3);
		} else {
			errnum = AppError.UNKNOWN;
			msg = fault.faultstring;
		}
		return new AppError(errnum, msg);
	}
}
