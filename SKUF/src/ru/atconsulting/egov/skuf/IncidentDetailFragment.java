package ru.atconsulting.egov.skuf;

import org.ksoap2.serialization.SoapObject;

import ru.atconsulting.egov.skuf.remedy.RemedyObjectLoadAsyncTask;
import ru.atconsulting.egov.skuf.remedy.RemedyObjectType;
import ru.atconsulting.egov.skuf.remedy.artifacts.RemedyIncident;
import ru.atconsulting.egov.skuf.util.RemedyObjectDetailFragmentFactory;
import ru.atconsulting.egov.skuf.util.SoapDateTimeParser;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class IncidentDetailFragment extends RemedyObjectDetailFragment<RemedyIncident> {
	
	static {
		RemedyObjectDetailFragmentFactory.getInstance().register(RemedyObjectType.INCIDENT, new IncidentDetailFragment());
	}
	
	public IncidentDetailFragment() {
		
	}
	
	public RemedyObjectDetailFragment<RemedyIncident> createFragment() {
		return new IncidentDetailFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {		
		View view = inflater.inflate(R.layout.incident_detail_fragment, container, false);		
		return view;
	}

	private class IncidentDetailLoadTask extends RemedyObjectLoadAsyncTask<RemedyIncident> {
		
		private static final String METHOD = "GetIncident";
		private static final String WSNAME = "ATC:SKUFAPP:Incident";
		private static final String ELEMENT = "IncidentNumber";
		
		public IncidentDetailLoadTask() {
			super(getActivity(), WSNAME, METHOD);
		}

		@Override
		protected RemedyIncident processSoapResult(Object result) {
			if (result instanceof SoapObject) {
				return makeIncident((SoapObject) result);
			}
			return null;
		}
		
		private RemedyIncident makeIncident(SoapObject obj) {
			RemedyIncident inc = new RemedyIncident();
			inc.setAssignedGroup(obj.getPrimitivePropertyAsString("AssignedGroup"));
			inc.setAssignee(obj.getPrimitivePropertyAsString("Assignee"));
			inc.setCreateDate(SoapDateTimeParser.parse(obj.getPrimitivePropertyAsString("SubmitDate")));
			inc.setDescription(obj.getPrimitivePropertyAsString("Description"));
			inc.setId(obj.getPrimitivePropertyAsString("IncidentNumber"));
			inc.setPriority(obj.getPrimitivePropertyAsString("Priority"));
			inc.setStatus(obj.getPrimitivePropertyAsString("Status"));
			inc.setUrgency(obj.getPrimitivePropertyAsString("Urgency"));
			return inc;
		}

		@Override
		protected String getObjectIDElement() {
			return ELEMENT;
		}
		
		@Override
		protected void onPostExecute(LoaderResult<RemedyIncident> result) {
			showProgress(false);
			if (! result.isSuccessful()) {
				Toast toast = Toast.makeText(getContext(), result.getError().getMessage(), Toast.LENGTH_LONG);
				toast.show();
				return;
			}
			setObject(result.getResult());
		}
	}

	@Override
	protected RemedyObjectLoadAsyncTask<RemedyIncident> createLoadTask() {
		return new IncidentDetailLoadTask();
	}

	@Override
	protected void fillDetails(RemedyIncident inc) {
		((TextView) getView().findViewById(R.id.inc_detail_number)).setText(inc.getId());
		((TextView) getView().findViewById(R.id.inc_detail_assignee)).setText(inc.getAssignee());
		((TextView) getView().findViewById(R.id.inc_detail_description)).setText(inc.getDescription());		
	}	
}
