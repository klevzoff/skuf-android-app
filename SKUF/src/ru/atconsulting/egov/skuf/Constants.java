package ru.atconsulting.egov.skuf;

public interface Constants {
	
	// Preference keys
	public static final String PREF_KEY_USERNAME  = "pref_key_username";
	public static final String PREF_KEY_PASSWORD  = "pref_key_password";
	public static final String PREF_KEY_MIDTIER   = "pref_key_midtier";
	public static final String PREF_KEY_ARSERVER  = "pref_key_arserver";
	public static final String PREF_KEY_SAVEPWD   = "pref_key_savepwd";
	
	// Keys for internal data passing
	public static final String STR_KEY_USERNAME   = "username";
	public static final String STR_KEY_PASSWORD   = "password";
	public static final String STR_KEY_GROUPS     = "groups";
	public static final String STR_KEY_LISTTYPE   = "listtype";
	public static final String STR_KEY_OBJECTID   = "objectId";
	public static final String STR_KEY_OBJECTTYPE = "objectType";
	
	// Logging
	public static final String LOG_TAG            = "SKUF";
}
