package ru.atconsulting.egov.skuf;

public class AppError extends Exception {

	private static final long serialVersionUID = -6443051203989968205L;

	public static final int UNKNOWN = 0;
	public static final int AUTH_ERROR = 623;
	public static final int NO_RECORDS = 302;
	public static final int QUAL_ERR = 4558;
	
	private int errnum;
	
	public AppError(int errnum, String msg) {
		super(msg);
		this.errnum = errnum;
	}
	
	public AppError(String msg) {
		super(msg);
		this.errnum = UNKNOWN;
	}
	
	public int getErrnum() {
		return this.errnum;
	}
	
	public boolean isRemedyError() {
		return this.errnum == UNKNOWN;
	}
}
