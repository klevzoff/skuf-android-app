package ru.atconsulting.egov.skuf.remedy;

public enum RemedyObjectType {
	INCIDENT,
	CHANGE_REQUEST,
	TASK,
	PROBLEM_INVESTIGATION,
	KNOWN_ERROR,
	SERVICE_REQUEST;
	
	public String toString() {
		switch(this) {
			case INCIDENT:
			{
				return "��������";
			}
			case CHANGE_REQUEST:
			{
				return "���������";
			}
			case TASK:
			{
				return "�������";
			}
			case PROBLEM_INVESTIGATION:
			{
				return "��������";
			}
			case KNOWN_ERROR:
			{
				return "��������� ������";
			}
			case SERVICE_REQUEST:
			{
				return "������ �� ������������";
			}
			default: {
				return "";
			}
		}
	}
}
