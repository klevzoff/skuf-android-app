package ru.atconsulting.egov.skuf.remedy;

import java.util.List;

import ru.atconsulting.egov.skuf.R;
import ru.atconsulting.egov.skuf.remedy.artifacts.RemedyObject;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RemedyDataListAdapter extends ArrayAdapter<RemedyObject> {
	
	LayoutInflater mInflater;
	
	public RemedyDataListAdapter(Context context) {
		super(context, R.layout.ovc_list_element, R.id.ovc_listelem_obj_number);
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@SuppressLint("NewApi")
	public void setData(List<RemedyObject> data) {
		clear();
		if (data != null) {
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
				addAll(data);
			} else {
				for (RemedyObject o : data) {
					add(o);
				}
			}
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.ovc_list_element, parent, false);
		}
			
		RemedyObject obj = getItem(position);
		
		((TextView)convertView.findViewById(R.id.ovc_listelem_obj_number)).setText(obj.getId());
		((TextView)convertView.findViewById(R.id.ovc_listelem_status)).setText(obj.getStatus());
		((TextView)convertView.findViewById(R.id.ovc_listelem_assigned)).setText((obj.getAssignee().equals("")) ? obj.getAssignedGroup() : obj.getAssignee());
		((TextView)convertView.findViewById(R.id.ovc_listelem_description)).setText(obj.getDescription());
		
		return convertView;
	}

}
