package ru.atconsulting.egov.skuf.remedy.artifacts;

import java.util.Date;

import ru.atconsulting.egov.skuf.remedy.RemedyObjectType;

public class RemedyObject {
	private RemedyObjectType type;
	private String id;
	private String root_id;
	private String assignee;
	private String assignedGroup;
	private String description;
	private String details;
	private String urgency;
	private String status;
	private String priority;
	private Date createDate;
	
	public RemedyObject() {
		
	}
	
	public RemedyObject(RemedyObjectType type, String id, String assignee, String assigned_group, 
			String description, String urgency, String status, String priority) {
		this.setType(type);
		this.setId(id);
		this.setAssignee(assignee);
		this.setAssignedGroup(assigned_group);
		this.setDescription(description);
		this.setUrgency(urgency);
		this.setStatus(status);
		this.setPriority(priority);
	}
	
	public String toString() {
		return getId();
	}
	
	public RemedyObjectType getType() {
		return type;
	}
	public void setType(RemedyObjectType type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public String getAssignedGroup() {
		return assignedGroup;
	}
	public void setAssignedGroup(String assigned_group) {
		this.assignedGroup = assigned_group;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrgency() {
		return urgency;
	}
	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getRoot_id() {
		return root_id;
	}

	public void setRoot_id(String root_id) {
		this.root_id = root_id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
