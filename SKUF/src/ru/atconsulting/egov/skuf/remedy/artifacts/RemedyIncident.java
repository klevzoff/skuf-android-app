package ru.atconsulting.egov.skuf.remedy.artifacts;

import ru.atconsulting.egov.skuf.remedy.RemedyObjectType;

public class RemedyIncident extends RemedyObject {
	private String incidentType;
	
	public RemedyIncident() {
		super();
		setType(RemedyObjectType.INCIDENT);
	}

	public String getIncidentType() {
		return incidentType;
	}

	public void setIncidentType(String incidentType) {
		this.incidentType = incidentType;
	}
}
