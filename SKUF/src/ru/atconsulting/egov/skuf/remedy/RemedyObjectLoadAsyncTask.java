package ru.atconsulting.egov.skuf.remedy;

import android.content.Context;
import ru.atconsulting.egov.skuf.remedy.artifacts.RemedyObject;
import ru.atconsulting.egov.skuf.util.NameValuePair;

public abstract class RemedyObjectLoadAsyncTask<T extends RemedyObject> extends RemedyLoadAsyncTask<T> {
	
	public RemedyObjectLoadAsyncTask(Context context, String wsName, String method) {
		super(context, wsName, method);
	}
	
	protected abstract String getObjectIDElement();
	
	public final void execute(String objectId) {
		execute(new NameValuePair(getObjectIDElement(), objectId));
	}
	
}
