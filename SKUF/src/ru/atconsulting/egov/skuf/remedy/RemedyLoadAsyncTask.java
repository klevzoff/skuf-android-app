package ru.atconsulting.egov.skuf.remedy;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;

import ru.atconsulting.egov.skuf.AppError;
import ru.atconsulting.egov.skuf.Constants;
import ru.atconsulting.egov.skuf.LoaderResult;
import ru.atconsulting.egov.skuf.util.NameValuePair;
import ru.atconsulting.egov.skuf.util.SoapUtils;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

public abstract class RemedyLoadAsyncTask<Result> extends AsyncTask<NameValuePair, Void, LoaderResult<Result>> {
	
	private String mURL;
	private String mMethod;
	private String mNamespace;
	private String mUsername;
	private String mPassword;
	private Context mContext;
	
	public RemedyLoadAsyncTask(Context context, String wsName, String method) {
		this.mNamespace = "urn:" + wsName;
		this.mMethod = method;
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		this.mURL = pref.getString(Constants.PREF_KEY_MIDTIER, "") 
				+ "/arsys/services/ARService?server=" 
				+ pref.getString(Constants.PREF_KEY_ARSERVER, "") 
				+ "&webService=" + wsName;
		this.mUsername = pref.getString(Constants.PREF_KEY_USERNAME, "");
		this.mPassword = pref.getString(Constants.PREF_KEY_PASSWORD, "");
		this.mContext = context;
	}
	
	protected final Context getContext() {
		return this.mContext;
	}

	protected abstract Result processSoapResult(Object result);

	@Override
	protected final LoaderResult<Result> doInBackground(NameValuePair... params) {
		
		SoapObject request = new SoapObject(mNamespace, mMethod);
		for (NameValuePair nv : params) {
			request.addProperty(nv.getName(), nv.getValue().toString());
		}
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		
		SoapUtils.makeAuthHeader(envelope, mNamespace, mUsername, mPassword);
		
		Result result = null;
		try {
			Object response = SoapUtils.makeCall(mContext, mURL, mNamespace, mMethod, envelope);
			result = processSoapResult(response);
		} catch (AppError e) {
			return new LoaderResult<Result>(e);
		}
		
		return new LoaderResult<Result>(result);
	}
	
	
}
