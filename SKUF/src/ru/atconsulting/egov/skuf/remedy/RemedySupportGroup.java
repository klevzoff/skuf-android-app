package ru.atconsulting.egov.skuf.remedy;

public class RemedySupportGroup {
	private String mId;
	private String mName;
	private String mOrganization;
	private String mCompany;
	
	public RemedySupportGroup() {
		
	}
	
	public RemedySupportGroup(String id, String name, String organization, String company) {
		this.setId(id);
		this.setName(name);
		this.setOrganization(organization);
		this.setCompany(company);
	}

	public String getId() {
		return mId;
	}

	public void setId(String id) {
		this.mId = id;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

	public String getOrganization() {
		return mOrganization;
	}

	public void setOrganization(String organization) {
		this.mOrganization = organization;
	}

	public String getCompany() {
		return mCompany;
	}

	public void setCompany(String company) {
		this.mCompany = company;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof RemedySupportGroup) {
			return this.getId().equals(((RemedySupportGroup)o).getId());
		} else {
			return super.equals(o);
		}
	}
}
