package ru.atconsulting.egov.skuf.remedy;

import java.util.List;

public class RemedyUser {
	private String mUsername;
	private String mPassword;
	private List<RemedySupportGroup> mGroups;
	
	public RemedyUser() {
		
	}
	
	public RemedyUser(String username, String password, List<RemedySupportGroup> groups) {
		this.setUsername(username);
		this.setPassword(password);
		this.setGroups(groups);
	}

	public String getUsername() {
		return mUsername;
	}

	public void setUsername(String mUsername) {
		this.mUsername = mUsername;
	}

	public String getPassword() {
		return mPassword;
	}

	public void setPassword(String mPassword) {
		this.mPassword = mPassword;
	}

	public List<RemedySupportGroup> getGroups() {
		return mGroups;
	}

	public void setGroups(List<RemedySupportGroup> mGroups) {
		this.mGroups = mGroups;
	}
}
