package ru.atconsulting.egov.skuf.remedy;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;

import ru.atconsulting.egov.skuf.AppError;
import ru.atconsulting.egov.skuf.LoaderResult;
import ru.atconsulting.egov.skuf.remedy.artifacts.RemedyIncident;
import ru.atconsulting.egov.skuf.remedy.artifacts.RemedyObject;
import ru.atconsulting.egov.skuf.util.SoapDateTimeParser;
import ru.atconsulting.egov.skuf.util.SoapUtils;
import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

public class RemedyObjectListLoader extends AsyncTaskLoader<LoaderResult<List<RemedyObject>>> {
	
	public enum QualType {
		MY,
		MY_GROUPS,
		MY_GROUPS_FILTERED,
		MY_RESOLVED;
	}
	
	private static class QualBuilder {
		
		private static final String FALSEQUAL = "1 = 2";
		
		public static String buildQual(QualType type, RemedyUser user, List<RemedySupportGroup> filterGroups) {
			switch (type) {
				case MY: 
					return MessageFormat.format("''Assignee Login ID'' = \"{0}\" AND ''Status'' < 4", user.getUsername());
				case MY_RESOLVED:
					return MessageFormat.format("''Assignee Login ID'' = \"{0}\" AND ''Status'' = 4", user.getUsername());
				case MY_GROUPS: {
					StringBuilder qual = new StringBuilder();
					qual.append("(");
					if (user.getGroups().size() > 0) {
						qual.append("'Assigned Group ID' = \"").append(user.getGroups().get(0).getId()).append("\"");
						for (int i = 1; i < user.getGroups().size(); i++) {
							qual.append(" OR 'Assigned Group ID' = \"").append(user.getGroups().get(i).getId()).append("\"");
						}
						qual.append(") AND 'Status' < 4");
						return qual.toString();
					} else {
						return FALSEQUAL;
					}
				}
				case MY_GROUPS_FILTERED: {
					if (filterGroups == null) return FALSEQUAL;
					StringBuilder qual = new StringBuilder();
					if (user.getGroups().size() > 0 && filterGroups.size() > 0) {
						qual.append("(");
						for (RemedySupportGroup group : user.getGroups()) {
							if (filterGroups.contains(group)) {
								qual.append("'Assigned Group ID' = \"").append(user.getGroups().get(0).getId()).append("\"");
								break;
							}
						}
						for (int i = 1; i < user.getGroups().size(); i++) {
							if (filterGroups.contains(user.getGroups().get(i))) {
								qual.append(" OR 'Assigned Group ID' = \"").append(user.getGroups().get(i).getId()).append("\"");
							}
						}
						qual.append(") AND 'Status' < 4");
						return qual.toString();
					} else {
						return FALSEQUAL;
					}
				}
				default: return FALSEQUAL;
			}
		}
		
	}
	
	private static final String URL = "http://skuf.gosuslugi.ru/arsys/services/ARService?server=skuf-ar&webService=ATC:SKUFAPP:Incident";
	private static final String METHOD = "GetIncidentList";
	private static final String NAMESPACE = "urn:ATC:SKUFAPP:Incident";
	
	private String mQual;
	private QualType mQualtype;
	private Context mContext;
	private RemedyUser mUser;
	
	LoaderResult<List<RemedyObject>> mResult;
	List<RemedySupportGroup> mFilterGroups;
	
	public RemedyObjectListLoader(Context context, RemedyUser user, QualType qualtype, List<RemedySupportGroup> filterGroups) {
		super(context);
		this.mContext = context;
		this.mUser = user;
		this.mQualtype = qualtype;
		this.mFilterGroups = filterGroups;
		this.mQual = QualBuilder.buildQual(qualtype, user, filterGroups);
	}
	
	public RemedyObjectListLoader(Context context, RemedyUser user, QualType qualtype) {
		this(context, user, qualtype, null);
	}
	
	public void setFilterGroups(List<RemedySupportGroup> groups) {
		this.mFilterGroups = groups;
		this.mQual = QualBuilder.buildQual(this.mQualtype, this.mUser, groups);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public LoaderResult<List<RemedyObject>> loadInBackground() {
		
		List<RemedyObject> list = new ArrayList<RemedyObject>();
		
		SoapObject request = new SoapObject(NAMESPACE, METHOD);
		request.addProperty("Qualification", mQual);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		
		SoapUtils.makeAuthHeader(envelope, NAMESPACE, mUser.getUsername(), mUser.getPassword());
		
		Object result = null;
		try {
			result = SoapUtils.makeCall(mContext, URL, NAMESPACE, METHOD, envelope);
		} catch (AppError e) {
			if (e.getErrnum() == AppError.NO_RECORDS) {
				return new LoaderResult<List<RemedyObject>>(list);
			}
			return new LoaderResult<List<RemedyObject>>(e);
		}
		
		if (result instanceof Vector) {
			for (Object obj: (Vector) result) {
				if (obj instanceof SoapObject) {
					RemedyObject inc = makeRemedyObject((SoapObject) obj);	
					list.add(inc);
				}
			}
		}
		else if (result instanceof SoapObject) {
			RemedyObject inc = makeRemedyObject((SoapObject) result);				
			list.add(inc);
		}
		
		return new LoaderResult<List<RemedyObject>>(list);
	}
	
	private RemedyObject makeRemedyObject(SoapObject obj) {
		RemedyObject object = new RemedyIncident();
		object.setAssignedGroup(obj.getPrimitivePropertyAsString("AssignedGroup"));
		object.setAssignee(obj.getPrimitivePropertyAsString("Assignee"));
		object.setCreateDate(SoapDateTimeParser.parse(obj.getPrimitivePropertyAsString("SubmitDate")));
		object.setDescription(obj.getPrimitivePropertyAsString("Description"));
		object.setId(obj.getPrimitivePropertyAsString("IncidentNumber"));
		object.setPriority(obj.getPrimitivePropertyAsString("Priority"));
		object.setStatus(obj.getPrimitivePropertyAsString("Status"));
		object.setUrgency(obj.getPrimitivePropertyAsString("Urgency"));
		return object;
	}

	@Override
	public void onCanceled(LoaderResult<List<RemedyObject>> data) {
		super.onCanceled(data);
	}

	@Override
	public void deliverResult(LoaderResult<List<RemedyObject>> data) {
		mResult = data;
		if (isStarted()) {
			super.deliverResult(data);
		}
	}

	@Override
	protected void onReset() {
		super.onReset();
		onStopLoading();
		if (mResult != null) {
			mResult = null;
		}
	}

	@Override
	protected void onStartLoading() {
		if (mResult != null) {
			deliverResult(mResult);
		}
		if (takeContentChanged() || mResult == null) {
			forceLoad();
		}
	}

	@Override
	protected void onStopLoading() {
		cancelLoad();
	}
	
	
}
