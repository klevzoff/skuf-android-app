package ru.atconsulting.egov.skuf;

import ru.atconsulting.egov.skuf.remedy.RemedyObjectLoadAsyncTask;
import ru.atconsulting.egov.skuf.remedy.artifacts.RemedyObject;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;

public abstract class RemedyObjectDetailFragment<T extends RemedyObject> extends Fragment {
	
	private String mObjectId;
	private T mObject;
	
	private ProgressBar mLoadProgress;
	private ScrollView mDetailForm;
	
	protected RemedyObjectLoadAsyncTask<T> mLoadTask;
	
	public abstract RemedyObjectDetailFragment<T> createFragment();	
	protected abstract RemedyObjectLoadAsyncTask<T> createLoadTask();
	protected abstract void fillDetails(T object);
	
	public T getObject() {
		return this.mObject;
	}
	
	public void setObject(T object) {
		this.mObject = object;
		fillDetails(object);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mLoadProgress = (ProgressBar) getView().findViewById(R.id.detail_load_progress);
		mDetailForm = (ScrollView) getView().findViewById(R.id.detail_form);
		
		mObjectId = getArguments().getString(Constants.STR_KEY_OBJECTID);
		
		if (mObject == null && mLoadTask == null) {	
			showProgress(true);
			mLoadTask = createLoadTask();
			mLoadTask.execute(mObjectId);
		}
	}	
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	protected void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoadProgress.setVisibility(View.VISIBLE);
			mLoadProgress.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoadProgress.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mDetailForm.setVisibility(View.VISIBLE);
			mDetailForm.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mDetailForm.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoadProgress.setVisibility(show ? View.VISIBLE : View.GONE);
			mDetailForm.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mLoadTask != null) {
			mLoadTask.cancel(true);
			mLoadTask = null;
		}
	}

}
