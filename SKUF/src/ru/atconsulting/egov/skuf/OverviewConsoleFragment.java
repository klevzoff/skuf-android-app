package ru.atconsulting.egov.skuf;

import java.util.List;

import ru.atconsulting.egov.skuf.remedy.RemedyDataListAdapter;
import ru.atconsulting.egov.skuf.remedy.RemedyObjectListLoader;
import ru.atconsulting.egov.skuf.remedy.RemedyObjectListLoader.QualType;
import ru.atconsulting.egov.skuf.remedy.RemedyUser;
import ru.atconsulting.egov.skuf.remedy.artifacts.RemedyObject;
import ru.atconsulting.egov.skuf.util.RemedyUserBundler;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

public class OverviewConsoleFragment extends Fragment {
	
	public interface RemedyUserProvider {
		public RemedyUser getUser();
	}

	SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    
    private static RemedyUser mUser;
    
    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mUser = ((RemedyUserProvider) getActivity()).getUser();
	}
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.overview_console_fragment, container, false);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getActivity().getSupportFragmentManager());

        mViewPager = (ViewPager) view.findViewById(R.id.pager);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(1);
        
        return view;
        
	}
    
	public class SectionsPagerAdapter extends FragmentPagerAdapter {
		 
		public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = new RemedyObjectListFragment();
            Bundle args = new Bundle();
            RemedyUserBundler.addToBundle(mUser, args);
            switch (position) {
            	case 0: {
            		args.putString(Constants.STR_KEY_LISTTYPE, RemedyObjectListLoader.QualType.MY_RESOLVED.toString());
            		break;
            	}
            	case 1: {
            		args.putString(Constants.STR_KEY_LISTTYPE, RemedyObjectListLoader.QualType.MY.toString());
            		break;
            	}
            	case 2: {
            		args.putString(Constants.STR_KEY_LISTTYPE, RemedyObjectListLoader.QualType.MY_GROUPS.toString());
            		break;
            	}
            }
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
        	switch (position) {
            case 0:
                return getString(R.string.title_section1);
            case 1:
                return getString(R.string.title_section2);
            case 2: 
            	return getString(R.string.title_section3);
        	}
        	
            return null;
        }
    }

    public static class RemedyObjectListFragment extends ListFragment implements LoaderCallbacks<LoaderResult<List<RemedyObject>>>, OverviewConsoleActivity.OnRefreshListener {
    	
    	private RemedyDataListAdapter mAdapter;
             
        public RemedyObjectListFragment() {
        }

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			((OverviewConsoleActivity) getActivity()).addOnRefreshListener(this);
			setEmptyText(getString(R.string.no_objects_to_display));
			getListView().setDivider(getResources().getDrawable(R.drawable.ovc_list_divider));
			getListView().setDividerHeight(2);
			mAdapter = new RemedyDataListAdapter(getActivity());
        	setListAdapter(mAdapter);
        	setListShown(false);
        	getLoaderManager().initLoader(0, getArguments(), this);
		}
		
		@Override
		public void onDestroy() {
			super.onDestroy();
			((OverviewConsoleActivity) getActivity()).removeOnRefreshListener(this);
		}

		@Override
		public Loader<LoaderResult<List<RemedyObject>>> onCreateLoader(int id, Bundle args) {
			return new RemedyObjectListLoader(getActivity(), RemedyUserBundler.readFromBundle(args), QualType.valueOf(args.getString(Constants.STR_KEY_LISTTYPE)));
		}

		@Override
		public void onLoadFinished(Loader<LoaderResult<List<RemedyObject>>> loader, LoaderResult<List<RemedyObject>> data) {
			if (data.isSuccessful()) {
				mAdapter.setData(data.getResult());
			} else {
				Toast toast = Toast.makeText(getActivity(), data.getError().getMessage(), Toast.LENGTH_LONG);
				toast.show();
			}
			if (isResumed()) {
				setListShown(true);
			} else {
				setListShownNoAnimation(true);
			}
		}

		@Override
		public void onLoaderReset(Loader<LoaderResult<List<RemedyObject>>> loader) {
			mAdapter.setData(null);
			
		}

		@Override
		public void onListItemClick(ListView l, View v, int position, long id) {
			if (getActivity() instanceof RemedyObjectListItemClickListener) {
				((RemedyObjectListItemClickListener) getActivity()).onListItemClick(mAdapter.getItem(position));
			}
		}
        
		public void onRefresh() {
			setListShown(false);
			getLoaderManager().restartLoader(0, getArguments(), this);
		}
        
    }
	
}
