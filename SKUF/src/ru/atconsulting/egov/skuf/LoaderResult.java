package ru.atconsulting.egov.skuf;

public class LoaderResult<T> {
	private boolean mSuccess;
	private T mResult;
	private Throwable mError;
	
	public LoaderResult(boolean success, T result) {
		this.mSuccess = success;
		this.mResult = result;
	}
	
	public LoaderResult(boolean success, Throwable error) {
		this.mSuccess = success;
		this.mError = error;
	}
	
	public LoaderResult(T result) {
		this(true, result);
	}
	
	public LoaderResult(Throwable error) {
		this(false, error);
	}
	
	public boolean isSuccessful() {
		return mSuccess;
	}
	
	public T getResult() {
		return mResult;
	}
	
	public Throwable getError() {
		return mError;
	}
}
