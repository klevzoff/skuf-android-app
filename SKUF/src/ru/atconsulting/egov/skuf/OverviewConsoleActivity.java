package ru.atconsulting.egov.skuf;

import java.util.ArrayList;
import java.util.List;

import ru.atconsulting.egov.skuf.remedy.RemedyUser;
import ru.atconsulting.egov.skuf.remedy.artifacts.RemedyObject;
import ru.atconsulting.egov.skuf.util.RemedyObjectDetailFragmentFactory;
import ru.atconsulting.egov.skuf.util.RemedyUserBundler;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;

public class OverviewConsoleActivity extends FragmentActivity implements OverviewConsoleFragment.RemedyUserProvider, RemedyObjectListItemClickListener {
	
	static {
		try {
			Class.forName("ru.atconsulting.egov.skuf.IncidentDetailFragment");
		} catch (ClassNotFoundException e) {

		}
	}
	
	public interface OnRefreshListener {
		public void onRefresh();
	}
	
	private List<OnRefreshListener> mRefreshableFragments;
	
	public void addOnRefreshListener(OnRefreshListener listener) {
		mRefreshableFragments.add(listener);
	}
	
	public void removeOnRefreshListener(OnRefreshListener listener) {
		mRefreshableFragments.remove(listener);
	}
	
	private RemedyUser mUser;
	
	private Fragment ovc_fragment;
	private Fragment detail_fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        
        mRefreshableFragments = new ArrayList<OverviewConsoleActivity.OnRefreshListener>();    
        
        mUser = RemedyUserBundler.readFromIntent(getIntent());
        
        setContentView(R.layout.activity_overview_console);
        
        if (savedInstanceState == null) {
        	ovc_fragment = new OverviewConsoleFragment();
        	ovc_fragment.setRetainInstance(true);
        	FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        	transaction.add(R.id.ovc_container, ovc_fragment);
        	transaction.commit();
        }
        
        ovc_fragment = getSupportFragmentManager().findFragmentById(R.id.overview_console_fragment);      
        detail_fragment = getSupportFragmentManager().findFragmentById(R.id.detail_fragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_overview_console, menu);
        return true;
    }

    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_refresh:
			refreshLists(item);
			return true;
		case R.id.menu_settings:
			openSettings(item);
			return true;
		default: return super.onOptionsItemSelected(item); 
		}
	}
    
    public void refreshLists(MenuItem item) {
    	for (OnRefreshListener listener: mRefreshableFragments) {
    		listener.onRefresh();
    	}
    }
    
    public void openSettings(MenuItem item) {
    	
    }

	@Override
	public RemedyUser getUser() {
		return mUser;
	}

	@Override
	public void onListItemClick(RemedyObject object) {
		detail_fragment = RemedyObjectDetailFragmentFactory.getInstance().createFragment(object.getType());
		Bundle args = new Bundle();
		args.putString(Constants.STR_KEY_OBJECTID, object.getId());
		detail_fragment.setArguments(args);
		detail_fragment.setRetainInstance(true);
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		if (getResources().getBoolean(R.bool.two_pane)) {
			transaction.replace(R.id.detail_fragment_container, detail_fragment);
		} else {
			transaction.replace(R.id.ovc_container, detail_fragment);
			transaction.addToBackStack(null);
		}
		transaction.commit();
		
	}
	
	

}
