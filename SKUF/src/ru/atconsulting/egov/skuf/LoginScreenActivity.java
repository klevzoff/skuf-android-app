package ru.atconsulting.egov.skuf;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.ksoap2.serialization.SoapObject;

import ru.atconsulting.egov.skuf.remedy.RemedyLoadAsyncTask;
import ru.atconsulting.egov.skuf.remedy.RemedySupportGroup;
import ru.atconsulting.egov.skuf.remedy.RemedyUser;
import ru.atconsulting.egov.skuf.util.RemedyUserBundler;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginScreenActivity extends FragmentActivity {

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// Values for email and password at the time of the login attempt.
	private String mUsername;
	private String mPassword;
	
	private boolean mSavePassword;
	
	SharedPreferences mPref;

	// UI references.
	private EditText mLoginView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;
	private CheckBox mSavePasswordView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login_screen);
		
		PreferenceManager.setDefaultValues(getApplication(), R.xml.preferences, false);
		mPref = PreferenceManager.getDefaultSharedPreferences(getApplication());
		
		mSavePassword = mPref.getBoolean(Constants.PREF_KEY_SAVEPWD, false);

		// Set up the login form.
		mLoginView = (EditText) findViewById(R.id.login);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int id,
					KeyEvent keyEvent) {
				if (id == R.id.login || id == EditorInfo.IME_NULL) {
					attemptLogin();
					return true;
				}
				return false;
			}
		});
		
		mSavePasswordView = (CheckBox) findViewById(R.id.save_password);
		
		if (mSavePassword) {
			mUsername = mPref.getString(Constants.PREF_KEY_USERNAME, "");
			mLoginView.setText(mUsername);
			mPassword = mPref.getString(Constants.PREF_KEY_PASSWORD, "");
			mPasswordView.setText(mPassword);
			mSavePasswordView.setChecked(mSavePassword);
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		}

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				attemptLogin();
			}
		});
		
		Object state = getLastCustomNonConfigurationInstance();
		if (state instanceof UserLoginTask) {
			mAuthTask = (UserLoginTask) state;
			showProgress(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.activity_login_screen, menu);
		return true;
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mLoginView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mUsername = mLoginView.getText().toString();
		mPassword = mPasswordView.getText().toString();
		mSavePassword = mSavePasswordView.isChecked();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mUsername)) {
			mLoginView.setError(getString(R.string.error_field_required));
			focusView = mLoginView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			SharedPreferences.Editor editor = mPref.edit();
			if (mSavePassword) {
				editor.putString(Constants.PREF_KEY_USERNAME, mUsername).putString(Constants.PREF_KEY_PASSWORD, mPassword).putBoolean(Constants.PREF_KEY_SAVEPWD, true);
			} 
			else if (mPref.getBoolean(Constants.PREF_KEY_SAVEPWD, false)) {
				editor.remove(Constants.PREF_KEY_USERNAME).remove(Constants.PREF_KEY_PASSWORD).putBoolean(Constants.PREF_KEY_SAVEPWD, false);
			}		
			editor.commit();
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask(this, mUsername, mPassword);
			mAuthTask.execute();
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	
	@Override
	public Object onRetainCustomNonConfigurationInstance() {
		return mAuthTask;
	}
	
	@Override
	public void onBackPressed() {
		if (mAuthTask != null) {
			mAuthTask.cancel(true);
			mAuthTask = null;
		}
		super.onBackPressed();
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	private class UserLoginTask extends RemedyLoadAsyncTask<List<RemedySupportGroup>> {
		
		private static final String METHOD = "GetUserGroupList";
		private static final String WSNAME = "ATC:SKUFAPP:User";				
		
		public UserLoginTask(Context context, String username, String password) {
			super(context, WSNAME, METHOD);
		}
		
		@SuppressWarnings("rawtypes")
		@Override
		protected List<RemedySupportGroup> processSoapResult(Object result) {
			
			List<RemedySupportGroup> list = new ArrayList<RemedySupportGroup>();
			
			if (result instanceof Vector) {
				for (Object obj: (Vector) result) {
					if (obj instanceof SoapObject) {
						RemedySupportGroup sg = makeSupportGroup((SoapObject) obj);					
						list.add(sg);
					}
				}
			} 
			else if (result instanceof SoapObject) {
				RemedySupportGroup sg = makeSupportGroup((SoapObject) result);					
				list.add(sg);
			}
			
			return list;
		}

		private RemedySupportGroup makeSupportGroup(SoapObject obj) {
			RemedySupportGroup sg = new RemedySupportGroup();
			sg.setId(obj.getPropertyAsString("GroupID"));
			sg.setName(obj.getPropertyAsString("GroupName"));
			sg.setOrganization(obj.getPropertyAsString("Organization"));
			sg.setCompany(obj.getPropertyAsString("Company"));					
			return sg;
		}

		@Override
		protected void onPostExecute(final LoaderResult<List<RemedySupportGroup>> result) {
			mAuthTask = null;
			if (result.isSuccessful()) {
				Intent intent = new Intent(LoginScreenActivity.this, OverviewConsoleActivity.class);
				RemedyUser user = new RemedyUser(mUsername, mPassword, result.getResult());
				RemedyUserBundler.addToIntent(user, intent);
				startActivity(intent);
				finish();
			} else {
				showProgress(false);
				if (result.getError() instanceof AppError) {
					if (((AppError)result.getError()).getErrnum() == AppError.AUTH_ERROR) {
						mPasswordView.setError(getString(R.string.error_incorrect_password));
						mPasswordView.requestFocus();
						return;
					}
				}
				Toast toast = Toast.makeText(getContext(), result.getError().getMessage(), Toast.LENGTH_LONG);
				toast.show();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
}
